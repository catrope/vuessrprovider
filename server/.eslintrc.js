module.exports = {
	root: true,
	extends: [
		'wikimedia/server',
		'plugin:@typescript-eslint/recommended',
		'plugin:@typescript-eslint/recommended-requiring-type-checking'
	],
	parser: '@typescript-eslint/parser',
	parserOptions: {
		parser: '@typescript-eslint/parser',
		sourceType: 'module',
		tsConfigRootDir: __dirname
	},
	plugins: [ '@typescript-eslint' ],
	rules: {
		'jsdoc/require-param-type': 'off',
		'jsdoc/require-returns-type': 'off',
		'@typescript-eslint/explicit-function-return-type': [
			'warn',
			{
				allowExpressions: true
			}
		]
	},
	settings: {
		jsdoc: {
			mode: 'typescript'
		}
	}
};
