import { ComponentOptions } from 'vue';
import * as Vue from 'vue';
import { parse, SFCParseResult } from 'vue/compiler-sfc';
import jquery from 'jquery';
import { JSDOM } from 'jsdom';
import makeMw from './mw';

let totalCompileTime = 0n;

export interface ModuleDefinition {
	files: Record<string, string | Record<string, unknown>>,
	entry: string,
	messages?: string, // JSON string that gets decoded TODO handle this more nicely
	dependencies?: string[]
};

export interface RequestExecutionContext {
	modules: Record<string, ModuleDefinition>,
	moduleExportsCache: Map<string, unknown>,
	messageMap: Map<string, string>,
	mockWindow: Record<string, unknown>
};

interface ModuleExecutionContext {
	moduleName: string,
	files: Record<string, string|Record<string, unknown>>,
	fileExportsCache: Map<string, unknown>,
	requestContext: RequestExecutionContext
}

const builtinModules = {
	vue: Vue
};

// resolveRelativePath was copied from resources/src/startup/mediawiki.js

// TODO maybe load the entire mediawiki.base module from RL, and let it handle all the module
// execution stuff? Then we'd just feed it mw.loader.implement calls, and get the result out
// with mw.loader.require( moduleName ). That would also bring along mw.config and the default
// user options for free, and generally remove the need to mock a bunch of mw.* stuff

/**
 * Resolve a relative file path.
 *
 * For example, resolveRelativePath( '../foo.js', 'resources/src/bar/bar.js' )
 * returns 'resources/src/foo.js'.
 *
 * @param relativePath Relative file path, starting with ./ or ../
 * @param basePath Path of the file (not directory) relativePath is relative to
 * @return Resolved path, or null if relativePath does not start with ./ or ../
 */
 function resolveRelativePath( relativePath: string, basePath: string ): string|null {
	const relParts = relativePath.match( /^((?:\.\.?\/)+)(.*)$/ );

	if ( !relParts ) {
		return null;
	}

	const baseDirParts = basePath.split( '/' );
	// basePath looks like 'foo/bar/baz.js', so baseDirParts looks like [ 'foo', 'bar, 'baz.js' ]
	// Remove the file component at the end, so that we are left with only the directory path
	baseDirParts.pop();

	const prefixes = relParts[ 1 ].split( '/' );
	// relParts[ 1 ] looks like '../../', so prefixes looks like [ '..', '..', '' ]
	// Remove the empty element at the end
	prefixes.pop();

	// For every ../ in the path prefix, remove one directory level from baseDirParts
	let prefix : string;
	while ( ( prefix = prefixes.pop() ) !== undefined ) {
		if ( prefix === '..' ) {
			baseDirParts.pop();
		}
	}

	// If there's anything left of the base path, prepend it to the file path
	return ( baseDirParts.length ? baseDirParts.join( '/' ) + '/' : '' ) + relParts[ 2 ];
}

/**
 * Execute a single file. For JS files, this executes the code in the file and returns
 * module.exports. For Vue files, this executes the code in the file and returns module.exports
 * plus a .template property set to the contents of the template. For JSON files, this returns
 * an object.
 * 
 * @param context Execution context
 * @param path File path
 * @returns module.exports of the executed file
 */
function executeFile( context: ModuleExecutionContext, path: string ) {
	let code = context.files[ path ];
	let result;
	if ( typeof code === 'string' ) {
		// For .js files, execute the code directly
		// For .vue files, execute the code, then add the template
		let parsedSFC: SFCParseResult = null;
		if ( path.endsWith( '.vue' ) && false ) {
			// FIXME this is not actually used because ResourceLoader does this for us
			/*parsedSFC = parse( code );
			// TODO check parsedSFC.errors
			code = parsedSFC.descriptor.script.content;*/
		}

		const moduleObj = { exports: {} };
		const mockWindow = context.requestContext.mockWindow;
		const compileStart = process.hrtime.bigint();
		// eslint-disable-next-line no-new-func
		const wrapperFunction = new Function(
			'module', 'exports', 'require', 'window', '$', 'jQuery',
			`with(window){${code}}`
		);
		totalCompileTime += process.hrtime.bigint() - compileStart;

		try {
			wrapperFunction.call(
				mockWindow,
				moduleObj,
				moduleObj.exports,
				// eslint-disable-next-line no-use-before-define
				makeRequireFunction( context, path ),
				mockWindow,
				mockWindow.jQuery,
				mockWindow.jQuery
			);
		} catch ( e ) {
			if ( !e._vue_ssr_prefixed ) {
				// TODO create stack trace with line numbers and file names that match real ones
				// and omit internal stuff inside this service. Doesn't have to be in e.stack,
				// can just be added to e.message after tracking manually
				e.message = `Error executing file ${path} in module ${context.moduleName}: ${e.message}`;
				e._vue_ssr_prefixed = true;
			}
			throw e;
		}
		if ( parsedSFC !== null && false ) {
			// FIXME unused
			/*( moduleObj.exports as ComponentOptions<never> ).template =
				parsedSFC.descriptor.template.content;*/
		}
		result = moduleObj.exports;
	} else {
		result = code;
	}
	context.fileExportsCache.set( path, result );
	return result;
}

/**
 * Make a require() function scoped to a specific file.
 * 
 * The returned function returns the module.exports of the requested file or module. File names are
 * looked up relative to the provided file path.
 *
 * @param context Execution context
 * @param path File path
 * @returns Function to be passed in as 'require' when executing the file
 */
function makeRequireFunction( context: ModuleExecutionContext, path: string ) {
	return function ( fileName: string ): unknown {
		const resolvedFileName = resolveRelativePath( fileName, path );
		if ( resolvedFileName === null ) {
			if ( fileName in builtinModules ) {
				return builtinModules[ fileName ];
			}
			if ( fileName in context.requestContext.modules ) {
				// eslint-disable-next-line no-use-before-define
				return executeModule( context.requestContext, fileName );
			}
			throw new Error( `Cannot require() undefined module ${fileName}` );
		}
		if ( !( resolvedFileName in context.files ) ) {
			throw new Error( `Cannot require() undefined file ${fileName}` );
		}
		if ( context.fileExportsCache.has( resolvedFileName ) ) {
			return context.fileExportsCache.get( resolvedFileName );
		}

		return executeFile( context, resolvedFileName );
	};
}

/**
 * Decode a JSON string with i18n messages (if needed), and add each message to messageMap
 * @param messageMap Map to add messages to
 * @param messages JSON string or object with i18n messages
 */
function addMessages( messageMap: Map<string, string>, messages: string ) {
	const decodedMessages: Record<string, string> = JSON.parse( messages );
	for ( const [ k, v ] of Object.entries( decodedMessages ) ) {
		messageMap.set( k, v );
	}
}

export function executeModule( requestContext: RequestExecutionContext, moduleName: string, recursionDepth = 0 ): unknown {
	if ( !requestContext.moduleExportsCache.has( moduleName ) ) {
		const totalStartTime = process.hrtime.bigint();

		const { files, entry, messages = null, dependencies = [] } =
			requestContext.modules[ moduleName ];

		for ( const dependency of dependencies ) {
			if ( !( dependency in builtinModules ) ) {
				executeModule( requestContext, dependency, recursionDepth + 1 );
			}
		}

		const ownStartTime = process.hrtime.bigint();

		const context: ModuleExecutionContext = {
			moduleName,
			files,
			fileExportsCache: new Map(),
			requestContext
		};
		if ( messages !== null ) {
			addMessages( requestContext.messageMap, messages );
		}

		const entryExport = executeFile( context, entry );
		requestContext.moduleExportsCache.set( moduleName, entryExport );

		const finishedTime = process.hrtime.bigint();
		console.log( `${Array(recursionDepth + 1).join('\t')}Executed ${moduleName}: own time ${( finishedTime - ownStartTime )/1000000n}ms, total time ${( finishedTime - totalStartTime )/1000000n}ms` );

		if ( recursionDepth === 0 ) {
			console.log( `Total compile time: ${totalCompileTime / 1000000n}ms` );
		}
	}
	return requestContext.moduleExportsCache.get( moduleName );
}

export function makeRequestContext(
	modules: Record<string, ModuleDefinition>,
	lang: string,
	url: string,
	config: Record<string, unknown>
): RequestExecutionContext {
	const mw = makeMw( lang );
	mw.config.set( config );
	const mockWindow = new JSDOM( '', { url } ).window;
	mockWindow.mw = mw;
	mockWindow.jQuery = mockWindow.$ = jquery( mockWindow );
	return {
		modules,
		messageMap: mw.messages,
		mockWindow,
		moduleExportsCache: new Map()
	}
}