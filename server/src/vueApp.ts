import { createSSRApp, App, ComponentOptions } from 'vue';
import { renderToString } from 'vue/server-renderer';
import makeI18nPlugin from './i18nPlugin';
import { RequestExecutionContext } from './rlModule';

type ComponentWithPrepareApp = ComponentOptions<never> & {
	prepareApp?: ( app: App ) => void
}

export async function renderApp(
	component: ComponentWithPrepareApp,
	props: Record<string, unknown>,
	requestContext: RequestExecutionContext
) : Promise<string> {
	const app = createSSRApp( component, props );
	if ( component.prepareApp ) {
		component.prepareApp( app );
	}
	app.use( makeI18nPlugin( requestContext.mockWindow ) );

	return renderToString( app );
}
