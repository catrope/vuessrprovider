import express from 'express';
import { renderToString } from 'vue/server-renderer';
import { executeModule, makeRequestContext, ModuleDefinition } from './rlModule';
import { renderApp } from './vueApp';

interface RequestData {
	modules: Record<string, ModuleDefinition>,
	lang: string,
	mainModule?: string,
	exportProperty?: string,
	props: Record<string, unknown>,
	config: Record<string, unknown>,
	url: string
}

const server = express();
server.use( express.json( { limit: '10MB' } ) );

server.post( '/render', async ( req, res ) => {
	const startTime = process.hrtime.bigint();
	const {
		modules,
		lang,
		mainModule = 'main',
		exportProperty = null,
		props = {},
		config = {},
		url
	} = ( req.body as RequestData );

	console.log( `start makeRequestContext: ${(process.hrtime.bigint() - startTime)/1000000n} ms` );
	const requestContext = makeRequestContext( modules, lang, url, config );
	console.log( `end makeRequestContext: ${(process.hrtime.bigint() - startTime)/1000000n} ms` );

	// TODO create a fake console object that proxies calls, logs them, and sends them back in the
	// HTTP response where they're then transmitted to the client and logged to the browser console
	// Set this object as fakeWindow.console and make sure it's read out even (especially) in the
	// error case below
	try {
		console.log( `start executeModule: ${(process.hrtime.bigint() - startTime)/1000000n} ms` );
		const mainExport = executeModule( requestContext, mainModule );
		console.log( `end executeModule: ${(process.hrtime.bigint() - startTime)/1000000n} ms` );
		const componentObject = exportProperty === null ? mainExport : mainExport[ exportProperty ];
		console.log( `start renderApp: ${(process.hrtime.bigint() - startTime)/1000000n} ms` );
		const html = await renderApp( componentObject, props, requestContext );
		console.log( `end renderApp: ${(process.hrtime.bigint() - startTime)/1000000n} ms` );
		res.json( {
			html
		} );
	} catch ( error ) {
		res.json( { error: error.toString() + '\n' + error.stack } );
		console.warn( error );
		// throw error;
	}
	console.log( `Satisified request in ${(process.hrtime.bigint() - startTime)/1000000n} ms` );
} );

server.listen( 8082 );
console.log( 'Listening on port 8082' );
