# VueSSRProvider

Prototype MediaWiki extension that provides Vue SSR support

This uses the SSRProvider feature in MediaWiki core, which is prototyped here:
https://gerrit.wikimedia.org/r/c/mediawiki/core/+/779971

## Installation

- Clone this repo in the `extensions/VueSSRProvider` directory
- In your MediaWiki installation, download [the SSRProvider patch](https://gerrit.wikimedia.org/r/c/mediawiki/core/+/779971)
  (using `git review -d 779971`)
- Enable the extension by adding `wfLoadExtension( 'VueSSRProvider' );` to your `LocalSettings.php` file.

```php
// .. the rest of your LocalSettings
wfLoadExtension( 'VueSSRProvider' );
```

## Usage

- Run the server: In the `extensions/VueSSRProvider/server` directory, run `npm install`, then `npm start`
- To see this extension in action, install the VueTest extension, download 
  [the SSR demo patch](https://gerrit.wikimedia.org/r/c/mediawiki/extensions/VueTest/+/787825)
  (using `git review -d 787825`) and go to the `Special:VueTest` page on your wiki

### MediaWiki-Vagrant
- If you're using MediaWiki-Vagrant, add a file to the `vagrant/settings.d`
  directory with the contents `<?php wfLoadExtension( 'VueSSRProvider' );`
- To start the server, you'll need to either run the `npm start` command inside
  the Vagrant VM, or make the server accessible to Vagrant by running `vagrant
  ssh -- -R 8082:localhost:8082` and keeping that shell open
### MediaWiki-Docker

If you are developing with [MediaWiki Docker](https://www.mediawiki.org/wiki/MediaWiki-Docker),
you'll want to define a `service` in your `docker-compose.override.yml` file:

```Dockerfile
# docker-compose.override.yml
version: '3.7'
services:
  # ... any other services you've defined
  vuessr:
    build: ./extensions/VueSSRProvider/server
    ports:
      - 8082:8082
```

You can use the included [Dockerfile](./server/Dockerfile) to set up this service, or you
can provid your own.

In your `LocalSettings.php` file, you'll need to override the default URL for
the Vue Renderer server with one that is visible inside Docker's internal network.

```php
// .. the rest of your LocalSettings
wfLoadExtension( 'VueSSRProvider' );
$wgSSRVueRendererServerUrl = "http://vuessr:8082/render";
```